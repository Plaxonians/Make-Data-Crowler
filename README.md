# Make-Data-Scrapper

This Module scraps All Data from Make Textfield Drop-Down.

### Webpage used : https://www.boattrader.com/sell/

This Module Dump's all the Scrapped Data into a CSV file Named "Data.csv"

## Process to use.

Update your system

*sudo apt-get update*

Provide the executable permission to driver_installer.sh

*sudo chmod -x driver_installer.sh*

execute driver_installer.sh

*./driver_installer.sh*

Install selenium

*pip3 install selenium*

Execute script

*python3 Dropdown_Data_crowler.py*


Thank you...
